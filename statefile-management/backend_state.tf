
terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket = "test-backend-remote-storage"
    key    = "king/terraform.tfstate"
    region = "us-east-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "test-backend-table"
  }
}